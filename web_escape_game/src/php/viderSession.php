<?php
  //Script permettant de vider la session de tous les éléments du jeu lorsque l'utilisateur revient sur la page d'accueil

  session_start();
  foreach ($_SESSION as $key =>$value){
    if ($key!="username" && $key!="active"){
      unset($_SESSION[$key]);
    }
  }

  header('Location: ../../public/index.php');

?>
