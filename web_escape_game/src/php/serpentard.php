<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link href="../../public/bootstrap/bootstrap-4.4.1/dist/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="../../public/css/serpentard.css">
    <link rel="icon" type="image/png" href="../../public/img/background/logo-noir.gif" />
    <title></title>
  </head>
  <body>
    <div class="row">
      <div class="col-2" id="bandeau">

      </div>
      <div class="col-6" id="message">
        <p>Au vu de tes résultats, le choixpeau t'as affecté à la maison Serpentard.</p>
        <p>Pour trouver l'emplacement du premier horcruxe, déchiffre le lieu suivant:</p>
        <?php
          //Script permettant de mélanger l'ordre des mots du lieu ainsi que les lettres

          $chaine = "Terrier des Weasley a l'Est d'Exeter";
          $tab = preg_split("/[\s,]+/",$chaine);
          shuffle($tab);
          $new_chaine = "";
          foreach ($tab as $car) {
            $tmp = str_split($car);
            shuffle($tmp);
            $new_chaine .= implode($tmp);
            $new_chaine .= "&nbsp";
          }
          echo "<p>".$new_chaine."</p>";
         ?>
         
        <a href="../html/new_jeu_osm.html">ouvrir la carte</a>
      </div>

      <div class="col-4" id="armoirie">

      </div>
    </div>

    <script src ="../../public/jquery/jquery-3.4.1.js" ></script>
    <script src="../../public/bootstrap/bootstrap-4.4.1/dist/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
