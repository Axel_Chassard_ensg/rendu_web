<?php
  //Script permettant de mettre à jour la base de données lorsque la partie est terminée

  session_start();

  $user = $_SESSION["username"];
  $user = strtolower($user);
  $temps = "0";

  if (isset($_SESSION["temps-restant"])){
    $temps=$_SESSION["temps-restant"];
  }
  $score = intval($temps)*100;

  //connexion à la bdd
  include('./connect.php');
  mysqli_set_charset($link, "utf8");

  //requête de récupération de l'id de l'utilisateur
  $requete1 = "SELECT idUtilisateur FROM utilisateur WHERE pseudo = '".$user."'";

  $result1 = mysqli_query($link,$requete1);
  $ligne1 = mysqli_fetch_assoc($result1);
  $idUtilisateur = intval($ligne1["idUtilisateur"]);


  //requête de récupération de l'id de la partie
  $requete3 = "SELECT idPartie AS idP FROM partie ORDER BY idPartie DESC LIMIT 1";

  $result3 = mysqli_query($link,$requete3);
  $ligne3 = mysqli_fetch_assoc($result3);
  $idPartie = intval($ligne3["idP"])+1;


  //requete d'insertion dans la table utilisateur
  $requete2 = "INSERT INTO partie(idPartie,niveau,score,temps,nbIndice,idUtilisateur) VALUES ($idPartie,1,$score,$temps,0,$idUtilisateur)";


  // echo $requete2;
  mysqli_query($link,$requete2);



  //requête de récupération du nombre de tentative d'un utilisateur
  $requete4 = "SELECT COUNT(*) AS nbPartie FROM partie WHERE idUtilisateur=$idUtilisateur";

  $result4 = mysqli_query($link,$requete4);
  $ligne4 = mysqli_fetch_assoc($result4);
  $tentative = intval($ligne4["nbPartie"]);



  //requête permettant de mettre à jour la table joue
  $requete5 = "INSERT INTO joue VALUES ($idUtilisateur,$idPartie,$tentative)";


  mysqli_query($link,$requete5);

  //Une fois ce script réalisé, on se redirige vers la page de gain
  header('Location: ./gagne.php');



  exit();


 ?>
