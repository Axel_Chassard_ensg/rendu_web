<?php
//script ayant pour but de recuperer les donnees saisies par l'utilisateur lors de l authentification
//et de les verifier avec la bases de donnees pour ensuite rediriger vers la bonne page

//Connexion a la base de donnees
include('./connect.php');

session_start();

$form=true;

// verifie si le login est bien saisie
if (isset($_POST["login"])){
  $login = $_POST["login"];
  $login = strtolower($login);
  if (empty($_POST["login"])){
    $form = false;
  }
}

// verifie si le mot de passe est bien saisie
if (isset($_POST["password"])){
  $pass = $_POST["password"];
  if (empty($_POST["password"])){
    $form = false;
  }
}

if ($form){
  // code d'interaction avec la base de données
  $requete = 'SELECT * FROM utilisateur WHERE pseudo="'.$login.'"';
  if ($result = mysqli_query($link,$requete)){
    $ligne = mysqli_fetch_assoc($result);
    if ($ligne["pswd"]!=$pass){
      $form = false;
      $login = "";
      $pass = "";

    }
    else{
      $_SESSION["username"]=$login;
      $_SESSION["active"]="oui";
    }
  }
}



mysqli_close($link);

// chargement de la page d'authentification si form pas ok
if ($form == false){
  header('Location: ../html/log_in.html');
}

// chargement de la page de jeu si form ok
else{
  header('Location: ../../public/index.php');
}

?>
