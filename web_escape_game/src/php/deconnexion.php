<?php
  //Script permettant de détruire la session lors de la déconnexion de l'utilisateur
  session_start();
  foreach ($_SESSION as $key =>$value){
      unset($_SESSION[$key]);
  }
  header('location: ../../public/index.php');
  exit;
?>
