<?php
  //Script permettant de récupérer le hall of shame depuis la base de données et de le renvoyer à la
  //requête fetch du fichier tableau_score.js

  session_start();

  if(isset($_POST["trie"])){
    $val_trie = $_POST["trie"];
  }

  include('./connect.php');

  mysqli_set_charset($link, "utf8");

  #On ajuste le tri sql en fonction de l'option choisie

  #requete SQL
  switch($val_trie){
    case "tentative":
      $sql_trie = " WHERE temps > 0 ORDER BY tentative ASC, score DESC, niveau ASC, temps ASC, nbIndice ASC";
      break;

    case "temps":
      $sql_trie = " WHERE temps > 0 ORDER BY temps ASC, score DESC, tentative ASC, niveau ASC, nbIndice ASC";
      break;

    case "nbIndice":
      $sql_trie = " WHERE temps > 0 ORDER BY nbIndice ASC ,score DESC, tentative ASC, niveau ASC, temps ASC";
      break;

    case "score":
      $sql_trie = " WHERE temps > 0 ORDER BY score DESC, tentative ASC, niveau ASC, temps ASC, nbIndice ASC";
      break;

    default:
      $sql_trie = " WHERE temps > 0 ORDER BY score DESC, tentative ASC, niveau ASC, temps ASC, nbIndice ASC";
      break;
  }

  $requete ="SELECT pseudo, score, tentative, niveau, temps, nbIndice FROM utilisateur JOIN partie USING (idUtilisateur) JOIN joue ON ((joue.idUtilisateurs = utilisateur.idUtilisateur) AND (joue.idParties=partie.idPartie))";
  $limit = " LIMIT 10";
  $requete.=$sql_trie;
  $requete.=$limit;

  #On récupère le résultat de la requête dans un tableau php que l'on renvoie plus loin au fetch via echo
  if ($result = mysqli_query($link, $requete)) {
    while ($ligne = mysqli_fetch_assoc($result)) {
      $tab[] = $ligne;
    }
  }
  echo json_encode($tab);
 ?>
