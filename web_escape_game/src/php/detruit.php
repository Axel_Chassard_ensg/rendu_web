<?php
//Script permettant d'indiquer dans la session qu'un élément est détruit

session_start();

if (isset($_POST)){
  foreach ( $_POST as $post => $val )  {
    $val.="_detruit";
    $_SESSION[$val]=true;
    }
  echo json_encode($_SESSION);
  }
?>
