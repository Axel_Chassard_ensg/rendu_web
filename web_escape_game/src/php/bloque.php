<?php
//Script permettant de débloquer une figurine
session_start();

//Pour chaque paramètre passer dans le post, on débloque la valeur
if (isset($_POST)){
  foreach ( $_POST as $post => $val )  {
    $val.="_bloque";
    $_SESSION[$val]=false;
    }
  echo json_encode($_SESSION);
  }
?>
