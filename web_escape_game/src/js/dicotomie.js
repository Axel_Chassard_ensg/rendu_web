//*********************************************************************************************
//Exécution principale
//*********************************************************************************************

function chargerSession(){
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      if (nom=="debut"){
        temps=r['debut'];
      }
    }
    if (temps==null){
      initMinuteur();
    }
  })
  .then(afficheMinuteur)
}

function initMinuteur(){
  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}

function afficheMinuteur(){
  // console.log("affcihe");
  // console.log(temps);
  var date = new Date();
  var min = document.getElementById("minuteur");
  if (temps!=null){
    var tps = 1200000-(date.getTime()-parseInt(temps));
    // console.log(tps);
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      setTimeout(afficheMinuteur,10);
    }
    else{
      // majSession("temps-restant",0);
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      .then(fetch("../php/fin.php")
      // .then(r=>r.text())
      // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../html/voldemort.html"))
    }

  }

}

function majSession(nom,valeur){
  // console.log('maj session');
  // console.log(nom);
  var data = new FormData();
  data.append(nom,valeur);

  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
  // .then(r=>console.log("majSession"))
}



//*********************************************************************************************
//Exécution principale
//*********************************************************************************************

var l1 = document.getElementById('l1');
var l2 = document.getElementById('l2');
var m1 = document.getElementById('m1');
var m2 = document.getElementById('m2');
var button = document.querySelector("button");

var latGringotts = 51.514;
var longGringotts = -0.088;

var temps=null;

//A chaque fois que l'utilisateur indique une latitude (de même par la suite pour la longitude),
//on lui indique s'il est en dessous, au dessus de la latitude ou s'il a renseigné la bonne
l1.addEventListener("input",function(e){
  var valLat = e.target.value;
  if (valLat<latGringotts) {
    m1.innerHTML = "C'est plus !";
  }
  else{
    if(valLat>latGringotts){
      m1.innerHTML = "C'est moins !";
    }
    else{
      if(valLat==latGringotts){
        m1.innerHTML = "C'est la bonne coordonnée !";
      }
      //On gère le cas d'une saisie autre qu'un nombre décimal
      else{
        m1.innerHTML = "Votre saisie n'est pas valide";
      }
    }
  }
})

l2.addEventListener("input",function(e){
  var valLong = e.target.value;
  if (valLong<longGringotts) {
    m2.innerHTML = "C'est plus !";
  }
  else{
    if(valLong>longGringotts){
      m2.innerHTML = "C'est moins !";
    }
    else{
      if(valLong==longGringotts){
        m2.innerHTML = "C'est la bonne coordonnée !";
      }
      else{
        m2.innerHTML = "Votre saisie n'est pas valide";
      }
    }
  }
})

chargerSession();
afficheMinuteur();
