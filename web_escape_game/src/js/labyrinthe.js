//Script permettant d'exécuter les fonctionnalités du labyrinthe

//****************************************************************************************************************
//Fonction
//****************************************************************************************************************

function chargerSession(){
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      if (nom=="debut"){
        temps=r['debut'];
      }
    }
    if (temps==null){
      initMinuteur();
    }
  })
  .then(afficheMinuteur)
}

function initMinuteur(){
  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}

function afficheMinuteur(){
  // console.log("affcihe");
  // console.log(temps);
  var date = new Date();
  var min = document.getElementById("minuteur");
  if (temps!=null){
    var tps = 1200000-(date.getTime()-parseInt(temps));
    // console.log(tps);
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      setTimeout(afficheMinuteur,10);
    }
    else{
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      .then(fetch("../php/fin.php")
      // .then(r=>r.text())
      // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../html/voldemort.html"))

    }

  }

}

function majSession(nom,valeur){
  // console.log('maj session');
  // console.log(nom);
  var data = new FormData();
  data.append(nom,valeur);

  return fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
  // .then(r=>console.log("majSession"))
}

function depart(positionNagini){
  ///Fonction permettant de créer les murs du labyrinthe ainsi qu'un curseur et un point d'arrivé
  ///
  ///param positionNagini : la position où se trouve Nagini dans le labyrinthe
  ///type positionNagini : chaîne de caractère
  ///no return



  creermur();

  //Initialisation de la case de départ
  //On place comme curseur une image de Neville
  var img = document.createElement("img");
  img.src="../../public/img/labyrinthe/neville.gif";
  document.getElementById("0,8").appendChild(img);
  document.form.positionL.value = 0;
  document.form.positionC.value = 8;

  //Initiation de la case de la fin;
  //On place une image de Nagini
  var img = document.createElement("img");
  img.src="../../public/img/labyrinthe/nagini.gif";
  document.getElementById(positionNagini).appendChild(img);
}

function creermur(){
  //Fonction permettant de créer les murs du labyrinthe
  ///
  ///no return

  // //On définit la position des murs

  var position = ["0,9","0,15","1,1","1,2","1,3","1,4","1,5","1,6","1,7","1,9","1,11","1,12","1,13","1,15","1,17","2,7","2,13","2,15","2,17","3,0","3,1","3,2","3,3","3,4","3,5","3,7","3,8","3,9","3,11","3,12","3,13","3,15","3,17","4,3","4,5","4,7","4,9","4,13","4,17","5,1","5,3","5,5","5,7","5,9","5,10","5,11","5,13","5,14","5,15","5,16","5,17","6,1","6,3","6,9","6,13","7,1","7,3","7,4","7,5","7,6","7,7","7,9","7,11","7,12","7,13","7,15","7,16","7,17","8,1","8,7","8,9","8,11","8,15","9,1","9,2","9,3","9,4","9,5","9,7","9,8","9,9","9,13","9,14","9,15","9,16","9,17","9,18","10,3","10,11","10,13","10,15","11,0","11,1","11,3","11,4","11,5","11,6","11,7","11,8","11,9","11,10","11,11","11,13","11,15","11,17","12,3","12,7","12,11","12,13","12,17","13,1","13,2","13,3","13,5","13,7","13,9","13,11","13,13","13,14","13,15","13,16","13,17","14,5","14,9","14,11"];


  for (var i = 0; i < position.length; i++) {
    //On affecte à chaque position la classe mur qui contiendra dans le .css une image de haie

    document.getElementById(position[i]).setAttribute("class","mur");
    document.getElementById(position[i]).id = "";
  }
}

function deplacement(dep,positionNagini){
  ///Fonction permettant d'effectuer le déplacement du curseur
  ///
  ///param dep : déplacement effectué par l'utilisateur
  ///type dep : chaîne de caractère
  ///param positionNagini : position où se trouve Nagini
  ///type positionNagini : chaîne de caractère
  ///no return

  //On récupère la position du curseur
  var c = document.form.positionC.value*1;
  var l = document.form.positionL.value*1;
  //En fonction du déplacement effectué, on affiche la nouvelle position
  if (dep == "bas" && check(l+1,c)){
    afficher(l+1,c,positionNagini);
  }
  if (dep == "droite" && check(l,c+1)){
    afficher(l,c+1,positionNagini);
  }
  if (dep == "haut" && check(l-1,c)){
    afficher(l-1,c,positionNagini);
  }
  if (dep == "gauche" && check(l,c-1)){
    afficher(l,c-1,positionNagini);
  }
}

function check(l,c){
  ///Fonction permettant de vérifier que la case sur laquelle on souhaite se déplacer n'est pas un mur
  ///
  ///param c : coordonnée colonne
  ///type c : caractère
  ///param l : coordonnée ligne
  ///type l : caractère
  ///return : boolean

  return (document.getElementById(l+","+c));
}

function afficher(L,C,positionNagini){
  ///Fonction permettant de mettre à jour la position du curseur
  ///
  ///param C : coordonnée colonne
  ///type C : caractère
  ///param L : coordonnée ligne
  ///type L : caractère
  ///param positionNagini : position où se trouve Nagini
  ///type positionNagini : chaîne de caractère
  ///no return

  //On commence par effacer l'ancienne position
  effacer(document.form.positionL.value,document.form.positionC.value);

  //On replace l'image de Neville à sa nouvelle position
  var img = document.createElement("img");
  img.src="../../public/img/labyrinthe/neville.gif";
  document.getElementById(L+","+C).appendChild(img);
  document.form.positionL.value = L;
  document.form.positionC.value = C;
  vérifier(positionNagini);
}

function effacer(L,C){
  ///Fonction permettant d'effacer la position précédente du curseur
  ///
  ///param C : coordonnée colonne
  ///type C : caractère
  ///param L : coordonnée ligne
  ///type L : caractère
  ///no return

  var tmp = document.getElementById(L+","+C);
  tmp.removeChild(tmp.querySelector("img"));

}

function vérifier(positionNagini){
  ///Fonction permettant de tester si on a atteint Nagini et si c'est le cas, alors on affiche la page de gain
  ///
  ///param positionNagini : position où se trouve Nagini
  ///type positionNagini : chaîne de caractère

  if (document.form.positionL.value+","+document.form.positionC.value == positionNagini){
    afficher_gain();
  }
}

function afficher_gain(){
  ///Fonction permettant d'ouvrir la page de gain
  ///
  ///no return

  window.location.href = "../html/gain_labyrinthe.html";

}


//****************************************************************************************************************
//Execution
//****************************************************************************************************************

//On définit la position de Nagini
var positionNagini = "14,10";

var body = document.querySelector("body");
body.onload = depart(positionNagini);

//Permet d'interagir avec les boutons de la page
var bouton_Haut = document.getElementById("Haut");
bouton_Haut.addEventListener("click",function(){
  deplacement("haut",positionNagini);
})

var bouton_Gauche = document.getElementById("Gauche");
bouton_Gauche.addEventListener("click",function(){
  deplacement("gauche",positionNagini);
})

var bouton_Droite = document.getElementById("Droite");
bouton_Droite.addEventListener("click",function(){
  deplacement("droite",positionNagini);
})

var bouton_Bas = document.getElementById("Bas");
bouton_Bas.addEventListener("click",function(){
  deplacement("bas",positionNagini);
})


var temps=null;

//Permet d'interagir avec les touches directionnelles du clavier
document.onkeydown = function(event){
  if (event.keyCode == 37 || event.keyCode==81){
    deplacement("gauche",positionNagini);
  }
  if (event.keyCode == 38 || event.keyCode==90){
    deplacement("haut",positionNagini);
  }
  if (event.keyCode == 39 || event.keyCode==68){
    deplacement("droite",positionNagini);
  }
  if (event.keyCode == 40 || event.keyCode==83){
    deplacement("bas",positionNagini);
  }
}


chargerSession();
afficheMinuteur();
