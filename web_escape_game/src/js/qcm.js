//Script permettant d'enchaîner le déroulement du qcm et d'afficher une page correspondant à la réussite des questions

//****************************************************************************************
//Fonction
//****************************************************************************************

function chargerSession(){
  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      if (nom=="debut"){
        temps=r['debut'];
      }
    }
    if (temps==null){
      initMinuteur();
    }
  })
  .then(afficheMinuteur)
}

function initMinuteur(){
  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}

function afficheMinuteur(){
  // console.log("affcihe");
  // console.log(temps);
  var date = new Date();
  var min = document.getElementById("minuteur");
  if (temps!=null){
    var tps = 1200000-(date.getTime()-parseInt(temps));
    // console.log(tps);
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      setTimeout(afficheMinuteur,10);
    }
    else{
      // majSession("temps-restant",0);
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      .then(fetch("../php/fin.php")
      // fetch("../php/fin.php")
      // .then(r=>r.text())
      // .then(r=>console.log("r : "+r))
      .then(window.location.href = "../html/voldemort.html"))
    }

  }

}

function majSession(nom,valeur){
  // console.log('maj session');
  // console.log(nom);
  var data = new FormData();
  data.append(nom,valeur);

  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
  // .then(r=>console.log("majSession"))
}

function recupVariables(){
  ///Fonction permettant d'affecter aux variables js "question", "compteur" et "valider" les valeurs
  ///renseignées dans la page par l'utilisateur
  ///
  ///no return

  question=document.getElementById("question");
  compteur=document.getElementById("compteur");
  valider=document.getElementById("valider");
}


function creerProposition(proposition,valeur,name){
  ///Fonction permettant de créer une proposition du QCM et de la placer dans la page
  ///
  ///param proposition : une proposition pour une question
  ///type proposition : chaîne de caractère
  ///param valeur : value que l'on va affecter à la proposition
  ///type valeur : chaîne de caractère
  ///param name : identifiant de la question
  ///type name : chaîne de caractère
  ///no return

  var li =document.createElement('li');
  var input = document.createElement('input');
  input.type="radio";
  input.name=name;
  input.value=valeur;
  var label =document.createElement('label');
  label.innerHTML=proposition;
  label.appendChild(input);
  li.appendChild(label);
  document.querySelector('ul').appendChild(li);
}


function initPage(){
  ///Fonction permettant de mettre à jour la page en fonction de l'ordinalité de la question que l'utilisateur doit traité
  ///
  ///no return

  switch(question.value){
    case "q1":
      document.getElementById("enonce").innerHTML='Quel est le véritable de nom de Celui-Dont-On-Ne-Doit-Pas-Prononcer-Le-Nom ?';
      creerProposition("Peter Petitgrow","0","q1");
      creerProposition("Albus Perceval Wulfric Brian Dumbledore","1","q1");
      creerProposition("Jar-Jar Binks","2","q1");
      creerProposition("Tom Elvis Jedusor","3","q1");
      break;
    case "q2":
      document.getElementById("enonce").innerHTML="Quel monstre utilise l'héritier de Salzar Serpentard pour attaquer les écoliers de Poudlard ?";
      creerProposition("Un griffon","0","q2");
      creerProposition("Un basilic","1","q2");
      creerProposition("Un chien à 3 têtes","2","q2");
      creerProposition("Un dindon","3","q2");
      break;
    case "q3":
      document.getElementById("enonce").innerHTML="Qui a annoté le livre de potion du prince de sang-mêlée";
      creerProposition("Severus Rogue","0","q3");
      creerProposition("Dolores Ombrage","1","q3");
      creerProposition("Mondingus Fletcher","2","q3");
      creerProposition("Argus Rusard","3","q3");
      break;
    case "fin":
      afficheGain();
      break;
    default:
      console.log("erreur dans la mise à jour du qcm");

  }
}


var sub = function(event){
  ///Fonction permettant d'empêcher l'envoie du formulaire lorsque l'utilisateur valide son choix
  ///
  ///param event : l'événement d'envoie du formulaire
  ///type event : événement
  ///no return

  var radios = document.getElementsByName(question.value);
  for(var i = 0; i < radios.length; i++){
    if(radios[i].checked){
      valeur = radios[i].value;
    }
  }
  event.preventDefault();
  verif();
  main();

}


function verif(){
  ///Fonction permettant de tester si l'utilisateur répond correctement, incrémente son total le cas échéant et passe à la question suivante
  ///
  ///no return

  var tmp = parseInt(compteur.value);
  switch (question.value){
    case "q1":
      if (valeur=="3"){
        tmp+=1;
        compteur.value=tmp.toString();
      }
      question.value="q2";
      break;
    case "q2":
      if(valeur=="1"){
        tmp+=1;
        compteur.value=tmp.toString();
      }
      question.value="q3";
      break;
    case "q3":
      if (valeur=="0"){
        tmp+=1;
        compteur.value=tmp.toString();

      }
      question.value="fin";
      break;
    default:
    infanticide(document.body);
  }
}

function infanticide(pere){
  // fontion permettant de supprimer tous les enfants d'une balise html
  //
  // param pere : balise html dont on veut supprimer les enfants
  // type pere : html object
  // console.log("infanticide");
  while(pere.firstElementChild) {
    pere.removeChild(pere.firstElementChild);
  }
}

function afficheGain() {
  ///Fonction permettant d'afficher une nouvelle page en fonction du résultat de l'utilisateur à ce qcm
  ///
  ///no return

  switch (compteur.value) {
    case "0":
      document.location.href="../html/pouffsoufle.html";
      break;
    case "1":
      document.location.href="../php/serpentard.php";
      break;
    case "2":
      document.location.href="../php/serdaigle.php";
      break;
    case "3":
      document.location.href="../html/gryffondor.html";
      break;
  }

}

//****************************************************************************************
//Exécution principale
//****************************************************************************************



var question = null;
var compteur = null;
var enonce = null;
var choix1 = null;
var choix2 = null;
var choix3 = null;
var choix4 = null;
var valider=null;
var valeur=null;
var temps=null;

function main(){
  var form = document.getElementById("qcm");
  form.addEventListener("submit",sub,true);
  chargerSession();
  recupVariables();
  afficheMinuteur();
  infanticide(document.querySelector('ul'));
  initPage();
}

main();
