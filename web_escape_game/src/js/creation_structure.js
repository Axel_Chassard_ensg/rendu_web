//Script de création du mur

//**********************************************************************************************
//Exécution
//**********************************************************************************************


//Chargement du labyrinthe
var hauteur = 15;
var largeur = 19;
var table = document.createElement("table");
var tbody = document.createElement("tbody");
tbody.id='laby';

var tr0 = document.createElement("tr");
tr0.classList.add("bordure");

//Création des bordures
for (i=0;i<largeur+2;i++){
  var td = document.createElement('td');
  td.classList.add("bordure");
  tr0.appendChild(td);

  }
  tbody.appendChild(tr0);

//Création des cases
for (i=0;i<hauteur;i++){
  var tr = document.createElement('tr');
  var td = document.createElement('td');
  td.classList.add("bordure");
  tr.appendChild(td);
  for (j=0;j<largeur;j++){
    var td = document.createElement('td');
    td.classList.add("chemin");
    td.id=i+","+j;
    tr.appendChild(td);
  }
  var td = document.createElement('td');
  td.classList.add("bordure");
  tr.appendChild(td);
  tbody.appendChild(tr);
}
var tr14 = document.createElement('tr');
tr14.classList.add('bordure');
//Création des bordures
for (i=0;i<largeur+2;i++){
  var td = document.createElement('td');
  td.classList.add('bordure');
  tr14.appendChild(td);

}
tbody.appendChild(tr14);
table.appendChild(tbody);

document.getElementById("laby").appendChild(table);
document.form.hauteur.value = hauteur;
document.form.largeur.value = largeur;
