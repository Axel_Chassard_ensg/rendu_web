//***********************************************************************************************************
//Fonction
//***********************************************************************************************************

function chargerSession(){
  ///Fonction permettant de recuperer la valeur du minuteur dans la session si cette
  /// dernière a deja ete creee sinon de l'instancier

  fetch("../php/session.php")
  .then(r=>r.json())
  .then(r=>{
    for (let nom in r){
      //recuperation de la valeur
      if (nom=="debut"){
        temps=r['debut'];
      }
    }
    //instanciation
    if (temps==null){
      initMinuteur();
    }
  })
  //affiche le minuteur
  .then(afficheMinuteur)
}


function initMinuteur(){
  ///Fonction permettant d'instancier le minuteur dans la session a la date actuelle

  var data = new FormData();
  var debut = new Date();
  temps=debut.getTime();
  data.append("debut",temps);
  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })
}


function afficheMinuteur(){
  ///Fonction permettant la gestion et l affichage du minuteur dans la page actuelle


  var date = new Date();
  var min = document.getElementById("minuteur");
  //verifie l'instanciation de temps
  if (temps!=null){
    var tps = 1200000-(date.getTime()-parseInt(temps));
    //verifie s'il reste encore du temps
    if (tps>=0){
      var tps_secondes = tps / 1000;
      var minutes = Math.floor(tps_secondes / 60);
      var secondes = Math.round((tps_secondes % 60)*1000)/1000;
      min.innerHTML=minutes + ":"+secondes;
      //appel recursif de la fonction pour un raffraichissement au centieme
      setTimeout(afficheMinuteur,10);
    }
    //temps ecoule
    else{
      //mise a jour de la varaible staockant le temps restant dans la sesison
      var data = new FormData();
      data.append("temps-restant",0);

      fetch("../php/majSession.php",{
        method : 'post',
        body : data
      })
      //maj de la bdd
      .then(fetch("../php/fin.php")
      //redirection vers la prochaine page
      .then(window.location.href = "../html/voldemort.html"))
    }
  }
}


function majSession(nom,valeur){
  ///fonction permettant de mettre a jour la session
  ///
  ///param nom : nom de la variable a mettre a jour
  ///type nom : string
  ///param valeur : valeur a stocker dans la session
  ///type valeur : boolean ou int ou string

  var data = new FormData();
  data.append(nom,valeur);

  fetch("../php/majSession.php",{
    method : 'post',
    body : data
  })

}



function creer_img(poufsouffle,numero){
  ///Fonction permettant d'ajouter une balise image à la page, placé aléatoirement.
  ///
  ///param poufsouffle : indique si l'image est la coupe de poufsouffle.
  ///type poufsouffle : boolean
  ///param numero : numéro de l'image, permet de savoir laquelle dupliquer
  ///type numero : entier
  ///return : l'image créé
  ///rtype : html balise <img>


  image = document.createElement("img");
  document.body.append(image);

  //On teste si la coupe est celle de Poufsouffle, si c'est le cas on lui attribue la classe vrai_coupe qu'elle seule possède,
  //sinon, on charge une image du répertoire des coupes et on lui attribue la classe "fausse_coupe"
  if(poufsouffle){
    image.className = "vrai_coupe";
    image.src = "../../public/img/coupe/poufsouffle.png";
  }
  else{
    image.className = numero;
    image.src = "../../public/img/coupe/coupe" + numero + ".png";
  }

  //On définit une largeur et une hauteur responsive à la largeur de la fenêtre
  image.style.width = window.innerWidth/8 + "px";
  image.style.height = "auto";

  image.addEventListener("click",action_click);
  image.addEventListener("mouseover",function(e){
    e.target.style.cursor = "pointer";
  })
  placer_image_aleatoirement(image);


  return image;
}


function placer_image_aleatoirement(image){
  ///Fonction permettant de placer une image aléatoirement dans la page
  ///
  ///param image : l'image à placer aléatoirement
  //type image : html balise <img>
  //no return

  //On place l'image aléatoirement en s'assurant qu'elle ne dépasse entièrement les dimensions de la fenêtre
  image.style.position = "absolute";
  image.style.left = Math.random()*(window.innerWidth-image.width) + "px";
  image.style.top = Math.random()*(3*window.innerHeight/4-hauteur_p) + hauteur_p + "px";
}


function action_click(event){
  ///Fonction qui va déclencher l'affichage du gain si l'image cliqué est la bonne,
  ///ou va dupliquer l'image cliqué si ce n'est pas la bonne
  ///
  ///param event : l'événement déclenché au clic sur une image
  ///type event : événement
  ///no return

  if(event.target.className == "vrai_coupe"){
        afficher_gain();
      }
  else{
        dupliquer(event.target);
        }
}


function dupliquer(image){
  ///Fonction permettant de dupliquer une image tout en la positionnant aléatoirement, elle et sa réplique
  ///
  ///param image : l'image à dupliquer
  ///type image : html balise <img>
  ///no return

  //On commence par positionner aléatoirement celle cliqué
  placer_image_aleatoirement(image);

  //Puis on ajoute la même image à la page
  tab[tab.length] = creer_img(false,image.className);

  }


function loop(){
  ///Fonction permettant de dupliquer une image aléatoirement toutes les deux secondes
  ///
  ///no return

  //On récupère une image aléatoirement dans celle de la page et on la duplique
  indice = Math.trunc(Math.random()*((tab.length-2)))+1;
  dupliquer(tab[indice]);
  setTimeout(loop,2000);
}


function afficher_gain(){
  ///Fonction permettant d'ouvrir la page de gain lorsque la coupe de Poufssouffle est trouvé

  window.location.href ="../html/gain_coupe.html";


}


function resize(){
  //Fonction permettant de redimensionner les images de la fenêtre si celle ci est amener à changer de dimension
  ///
  ///no return

  var images = document.body.querySelectorAll("img");
  for (let img in images){
    if (images[img].style != undefined){
      images[img].style.width = window.innerWidth/8 + "px";
      images[img].style.height="auto";
      placer_image_aleatoirement(images[img]);
    }
  }

}

//***********************************************************************************************************
//exécution principale
//***********************************************************************************************************


//On récupère la hauteur du premier paragraphe afin, par la suite, de générer aléatoirement des images sous
//ce paragraphe
var p = document.querySelector("p");
var hauteur_p = p.offsetHeight;

var temps=null;

//on ajoute à la console l'image de la coupe de Poufsouffle. On la stocke en même temps dans une variable
var poufsouffle = creer_img(true,0);

//on créé à la vollée des balises images que l'on remplit avec les coupes et que l'on place aléatoirement
var tab = [];
tab[0] = poufsouffle;
for (var i = 1; i < 10; i++) {
  tab[i] = creer_img(false,i);
}


chargerSession();
afficheMinuteur();

//on duplique des éléments toutes les 5 secondes pour rajouter de la difficulté au jeu
setTimeout(loop,2000);


//Si l'utilisateur va varrier la taille de la fenêtre, les positions sont regénérées en fonction de la nouvelle taille
window.onresize=resize;
