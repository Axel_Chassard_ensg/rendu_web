//script ayant pour but de trier le tableau des scores en fonction des
//options de trie choisie par l'utilisateur

// import {infanticide} from './module.js';

//on récupère dans une valeur le menu déroulant de trie
trie = document.getElementById('trie');
// console.log("trie : " + trie);
// console.log("valeur : " + trie.value);

//on récupère dans une variable le tableau de score
tableau = document.getElementById('tableau');

function maj(valeur){
  //fonction qui va remplir le tableau de score en fonction de la valeur sélectionné dans le menu déroulant de trie
  // param valeur : valeur sélectionné dans le menu déroulant de trie
  // type valeur : option

  //appel de la fonction infanticide qui va permettre de vider le tableau des valeurs qui sont actuellement dedans
  infanticide(document.querySelector("tbody"));

  //mise en forme des données à envoyer par ajax
  data = "trie="+valeur;
  var chemin = "../php/tableau_score.php";
  // console.log("chemin : " + chemin);

  fetch(chemin, {
    method: 'post',
    body: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(r => r.json())
  .then(r => {
    // pour chaque ligne obtenue par sql, on créé une ligne dans le tableau que l'on remplit avec les données récupérées
    var i = 1;
    for (elem in r){
      contenu = document.querySelector("tbody");
      ligne = document.createElement('tr');
      entete = document.createElement('th');
      entete.classList.add("table-dark");


      entete.innerHTML=i;
      ligne.appendChild(entete);
      for (key in r[elem]) {
        d = document.createElement('td');
        d.classList.add("table-light");
        d.innerHTML = r[elem][key];
        ligne.appendChild(d);
      }
      contenu.appendChild(ligne);
      tableau.appendChild(contenu);
      i++;
    }
  })

}

maj("");


//A chaque fois que l'utilisateur change son option de trie, on met à jour le tableau en appelant la fonction maj
trie.addEventListener('change',function(e){
  maj(e.target.value);
  // console.log(e.target.value);
})



function infanticide(pere){
  // fontion permettant de supprimer tous les enfants d'une balise HTML
  //
  // param pere : balise html dont on veut supprimer les enfants
  // type pere : balise HTML


  while(pere.firstChild) {
    pere.removeChild(pere.firstChild);
  }
}
