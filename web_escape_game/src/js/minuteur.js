//conception d'une classe minuteur basée sur un modèle d'implémentation Builder

class Minuteur {
  private int id;
  private temps_init int;
  private boolean actif;
  private int malus;

  public void setId(int id){
    this.id = id;
  }

  public void setMalus(int malus){
    this.malus = malus;
  }

  public void setTempsInit(int temps_init){
    this.temps_init = temps_init;
  }

  public void setActif(actif){
    this.actif = actif;
  }

  public int getTempsInit(){
    return this.temps_init;
  }

  public int getMalus(){
    return this.malus;
  }

  public boolean getActif(){
    return this.actif;
  }

  public function debutMinuteur(temps){
    var secondes = temps % 60;
    var dizaines = Math.floor(secondes/10);
    var unites = secondes % 10;
    var minutes = Math.floor(temps/60);

    document.getElementById("temps").innerHTML= "" + minutes + ":" + dizaines + ":" + unites;
  }

  function decompte(){
    if (temps>0){
      temps-=malus;
      debutMinuteur(temps);
      a = setTimeout(decompte,1000);
    }
    else{
      clearTimeout(a);
    }
  }

  decompte(temps);

}

/*Monteur abstrait*/
abstract class MonteurMinuteur{

  /*creation d'un minuteur basique*/
  protected Minuteur minuteur;

  public MonteurMinuteur(){
    minuteur = new Minuteur();
  }

  /*minuteur issu du montage*/
  public Minuteur getMinuteur(){
    return minuteur;
  }

  /*differentes parties du minuteur*/
  public abstract void monterTempsInit();
  public abstratc void monterActif();
  public abstract void monterMalus();

}

/*monteur concret d'un minuteur en mode facile*/
class MonteurMinuteurFacile extends MonteurMinuteur{

  public void monterTempsInit(){
    minuteur.setTempsInit(0);
  }

  public void monterActif(){
    minuteur.setActif(false);
  }

  public void monterMalus(){
    minuteur.setMalus(1);
  }

}



/*monteur concretd'un minuteur moyen*/
class MonteurMinuteurMoyen extends MonteurMinuteur{

  public void monterTempsInit(){
    minuteur.setTempsInit(600);
  }

  public void monterActif(){
    minuteur.setActif(true);
  }

  public void monterMalus(){
    minuteur.setMalus(1);
  }
}

/*monteur concret d'un minuteur difficile*/
class MonteurMinuteurDifficile extends MonteurMinuteur{

  public void monterTempsInit(){
    minuteur.setTempsInit(360);
  }

  public void monterActif(){
    minuteur.setActif(false);
  }

  public void monterMalus(){
    minuteur.setMalus(1);
  }
}

/*Directeur*/
class Timer{

  private MonteurMinuteur monteurMinuteur;

  public void setMonteurMinuteur(MonteurMinuteur mm){
    monteurMinuteur = mm;
  }

  pulic Minuteur getMinuteur(){
    return monteurMinuteur.getMinuteur();
  }

  public void construireMinuteur(){
    monteurMinuteur.monterTempsInit();
    monteurMinuteur.monterActif();
    monteurMinuteur.monterMalus();
  }

}

/*Exemple de creation de minuteur*/
// class ExempleMonteur{
//
//   public static void main ( String [] args ){
//
//     Timer timer = new Timer ();
//
//     MonteurMinuteurFacile monteurMinuteurFacile = new MonteurMinuteurFacile();
//     MonteurMinuteur monteurMinuteurMoyen = new MonteurMinuteurMoyen ();
//     MonteurMinuteurDifficile monteurMinuteurDifficile = new MonteurMinuteurDifficile();
//
//     timer . setMonteurMinuteur ( monteurMinuteurMoyen );
//     timer . construireMinuteur ();
//
//     Minuteur minuteur = timer . getMinuteur ();
//   }
// }
//
// export default Timer;
