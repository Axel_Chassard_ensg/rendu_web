-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2019 at 05:42 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `escape_game`
--
CREATE DATABASE IF NOT EXISTS `escape_game` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `escape_game`;

-- --------------------------------------------------------

--
-- Table structure for table `joue`
--

CREATE TABLE `joue` (
  `idUtilisateurs` int(11) NOT NULL,
  `idParties` int(11) NOT NULL,
  `tentative` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `joue`
--

INSERT INTO `joue` (`idUtilisateurs`, `idParties`, `tentative`) VALUES
(1, 1, 1),
(1, 5, 2),
(1, 6, 3),
(1, 7, 4),
(1, 8, 5),
(1, 11, 6),
(1, 13, 7),
(1, 15, 8),
(1, 18, 9),
(1, 20, 10),
(2, 2, 1),
(2, 3, 2),
(2, 4, 3),
(2, 9, 4),
(2, 10, 5),
(6, 12, 1),
(6, 14, 2),
(6, 16, 3),
(6, 19, 4),
(8, 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `partie`
--

CREATE TABLE `partie` (
  `idPartie` int(11) NOT NULL,
  `niveau` int(11) NOT NULL,
  `score` int(45) NOT NULL,
  `temps` int(11) NOT NULL,
  `nbIndice` int(11) NOT NULL,
  `idUtilisateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partie`
--

INSERT INTO `partie` (`idPartie`, `niveau`, `score`, `temps`, `nbIndice`, `idUtilisateur`) VALUES
(1, 1, 1000, 10, 0, 1),
(2, 1, 2000, 30, 1, 2),
(3, 1, 5000, 5, 0, 2),
(4, 1, 2500, 20, 3, 2),
(5, 1, 0, 0, 0, 1),
(6, 1, 0, 0, 0, 1),
(7, 1, 123680700, 1236807, 0, 1),
(8, 1, 123680700, 1236807, 0, 1),
(9, 1, 0, 0, 0, 2),
(10, 1, 121290300, 1212903, 0, 2),
(11, 1, 0, 0, 0, 1),
(12, 1, 0, 0, 0, 6),
(13, 1, 0, 0, 0, 1),
(14, 1, 0, 0, 0, 6),
(15, 1, 0, 0, 0, 1),
(16, 1, 0, 0, 0, 6),
(17, 1, 0, 0, 0, 8),
(18, 1, 0, 0, 0, 1),
(19, 1, 0, 0, 0, 6),
(20, 1, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUtilisateur` int(11) NOT NULL,
  `pseudo` varchar(45) NOT NULL,
  `pswd` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `pseudo`, `pswd`, `email`) VALUES
(1, 'alex', 'alex', 'alex@gmail.com'),
(2, 'axou', 'axou', 'axou@gmail.com'),
(3, 'jackie chan', 'jackie', 'jackie@gmail.com'),
(4, 'harry', 'potter', 'harry@gmail.com'),
(5, 'potter', 'harry', 'potter@gmail.com'),
(6, 'baptou', 'Baptou', 'batou.batou@gmail.com'),
(7, 'jar-jar', 'bins', 'jar@gmail.com'),
(8, 'neville jedusor', 'toad', 'toad@gmail.com'),
(9, 'chibreoptique', 'oui', 'florent.geniet@ensg.eu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `joue`
--
ALTER TABLE `joue`
  ADD PRIMARY KEY (`idUtilisateurs`,`idParties`),
  ADD KEY `idUtilisateur_idx` (`idUtilisateurs`),
  ADD KEY `idPartie_idx` (`idParties`);

--
-- Indexes for table `partie`
--
ALTER TABLE `partie`
  ADD PRIMARY KEY (`idPartie`),
  ADD KEY `idUtilisateur_idx` (`idUtilisateur`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idUtilisateur`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `partie`
--
ALTER TABLE `partie`
  MODIFY `idPartie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `joue`
--
ALTER TABLE `joue`
  ADD CONSTRAINT `idParties` FOREIGN KEY (`idParties`) REFERENCES `partie` (`idPartie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `idUtilisateurs` FOREIGN KEY (`idUtilisateurs`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `partie`
--
ALTER TABLE `partie`
  ADD CONSTRAINT `idUtilisateur` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
