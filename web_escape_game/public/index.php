<?php
session_start();
?>

<!DOCTYPE html>
<html lang=fr dir="ltr">
<head>
  <meta charset="utf-8" name="projet developpement d'un escape game web géographique">
  <title>Escape Game</title>

  <link href="/public/bootstrap/bootstrap-4.4.1/dist/css/bootstrap.css" rel="stylesheet">
  <script src="/public/jquery/jquery-3.4.1.js" ></script>
  <script src="/public/bootstrap/bootstrap-4.4.1/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="stylesheet" href="../public/css/index.css" type="text/css" media="screen"/>
  <link rel="icon" type="image/png" href="/public/img/background/logo-noir.gif" />



</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault2" aria-controls="navbarsExampleDefault2" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <a class="navbar-brand" href="#">
            <img id="logo" src="../../public/img/background/logo-blanc.gif" alt="">
          </a>

          <?php
          if (isset($_SESSION["active"])){
            echo '<li class="nav-item active">
            <a class="nav-link disabled" href="../src/html/sign_in.html">Sign_in <span class="sr-only"></span></a>
            </li>';
            echo '<li class="nav-item">
            <a class="nav-link disabled" href="../src/html/log_in.html" role="button" >Log_in <span class="sr-only"></a>
            </li>';
            echo '<li class="nav-item">
            <a class="nav-link" href="../src/html/tableau_score.html">Tableau des scores</a>
            </li>';
          }

          else{
            echo '<li class="nav-item active">
            <a class="nav-link " href="../src/html/sign_in.html">Sign_in <span class="sr-only"></span></a>
            </li>';

            echo '<li class="nav-item">
            <a class="nav-link" href="../src/html/log_in.html" role="button" >Log_in <span class="sr-only"></a>
            </li>';
            echo '<li class="nav-item">
            <a class="nav-link" href="../src/html/tableau_score.html">Tableau des scores</a>
            </li>';
          }
          ?>
        </ul>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="navbar-nav ml-5">
          <h3>Web Escape Game</h3>
        </ul>

      </div>
      <div class="navbar-collapse collapse justify-content-end" id="navbarsExampleDefault2">
        <ul class="navbar-nav navbar-right">
          <?php
          if (isset($_SESSION["active"])){

            echo '<li class="nav-item">
            <a class="nav-link" href="../src/php/deconnexion.php">Deconnexion</a>
            </li>';
          }
          else{
            echo '<li class="nav-item">
            <a class="nav-link disabled" href="../src/php/deconnexion.php">Deconnexion</a>
            </li>';
          }
          ?>
        </ul>
      </div>
    </nav>




  <div class="row pt-5 mt-4">
    <p class="text-center" id="choix">Choississez le scénario qui vous fait frissoner d'envie</p>
  </div>

<script type="text/javascript">
$('.carouselExampleIndicators').carousel()
</script>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="../../public/img/carousel/Potter.gif" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>Harry Potter</h5>
        <p>Plongez-vous dans l'univers d'Harry Potter au sein duquel vous allez devoir poursuivre la quête des horcruxes de Vous savez qui avant qu'il ne détruise le monde</p>
        <?php
        if (isset($_SESSION["active"])){
          echo '<p>
          <a class"btn btn-primary" href="../../src/html/qcm.html">Jouer</a>
          </p>';
        }
        else{
          echo '<p>
          <a class"btn btn-primary" href="../../src/html/log_in.html">Jouer</a>
          </p>';
        }

        ?>

      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="../../public/img/carousel/ForrestGump.gif" alt="Second slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>Forrest Gump</h5>
        <p>Venez revivre la vie exceptionnelle de Forrest Gump, un homme simple au destin hors du commun</p>
        <p>
          <a class"btn btn-primary disabled">Jouer</a>
        </p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="../../public/img/carousel/HungerGames.gif" alt="Third slide">
      <div class="carousel-caption d-none d-md-block">
        <h5>Hunger Games</h5>
        <p>Pourrez-vous survivre au fameux Hunger Games et réussir à vous enfuir ? Votre vie en dépend ! Attention vos choix qui pourront vous faire perdre un temps précieux voire perdre la vie</p>
        <p>
          <a class"btn btn-primary disabled">Jouer</a>
        </p>
      </div>
    </div>
</div>
<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
  <span class="carousel-control-next-icon" aria-hidden="true"></span>
  <span class="sr-only">Next</span>
</a>
</div>




<!-- <footer></footer> -->
</body>
</html>
